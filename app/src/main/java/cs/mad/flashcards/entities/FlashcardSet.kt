package cs.mad.flashcards.entities

data class FlashcardSet(val title: String, val card: Array<Flashcard>) {
    val fc = Flashcard()

    fun createSetOfSets(): Array<FlashcardSet> {
        return arrayOf(
            FlashcardSet("Set 1", fc.createSet()),
            FlashcardSet("Set 2", fc.createSet()),
            FlashcardSet("Set 3", fc.createSet()),
            FlashcardSet("Set 4", fc.createSet()),
            FlashcardSet("Set 5", fc.createSet()),
            FlashcardSet("Set 6", fc.createSet()),
            FlashcardSet("Set 7", fc.createSet()),
            FlashcardSet("Set 8", fc.createSet()),
            FlashcardSet("Set 9", fc.createSet()),
            FlashcardSet("Set 10", fc.createSet())
        )
    }
}