package cs.mad.flashcards.entities

data class Flashcard(val term: String = "", val definition: String = "") {
    fun createSet(): Array<Flashcard> {
        return arrayOf (
            Flashcard("var", "A keyword which signifies a mutable variable in Kotlin"),
            Flashcard("val", "A keyword which signifies an immutable variable in Kotlin"),
            Flashcard("init", "A Kotlin keyword used to create initializer blocks for a primary constructor"),
            Flashcard("return", "A keyword used to exit a function/method with a return value"),
            Flashcard("fun", "A keyword used to define a function in Kotlin"),
            Flashcard("arrayOf", "A utility function in Kotlin that creates an array object"),
            Flashcard(":", "A symbol used to specify a data type of variable and return type of a function"),
            Flashcard("XML", "Stands for eXtensible Markup Language. Used to create android views"),
            Flashcard("class", "A keyword used to specify a Kotlin class"),
            Flashcard("interface", "A keyword used to specify a Kotlin interface")
        )
    }
}