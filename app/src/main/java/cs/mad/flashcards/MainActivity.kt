package cs.mad.flashcards

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import cs.mad.flashcards.entities.Flashcard

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_layout)

        // Source: https://www.studytonight.com/android/android-listview
        val list: ListView = findViewById(R.id.listView)
        val cards = Flashcard().createSet()

        val adapter = ArrayAdapter(this, R.layout.list_item, R.id.textView, cards)
        list.adapter = adapter
    }
}